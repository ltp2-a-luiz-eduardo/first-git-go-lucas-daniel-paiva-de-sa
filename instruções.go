package main

type Name string

const(
    GITCONFIG    Name = "git config"
    GITINIT           = "git init"
    GITSTATUS         = "git status"
    GITADD            = "git add"
    GITCOMMIT         = "git commit"
    GITPULL           = "git pull"
    GITCLONE          = "git clone"
    )

var commandMap = map[Name]Instruction {
    GITCONFIG:         Instruction{"Permite ver e atribuir variáveis de configuração do gi geralmente utilizado quando você precisa iniciar a configuração do GIT ou para consultar quais são suas configurações atuais."},
    GITINIT:           Instruction{"Este comando pode ser utilizado para criar um novo repositório do Git ou para transformar um diretório em um repositório do Git."},
    GITSTATUS:         Instruction{"Exibe uma lista dos arquivos e diretórios que foram modificados e adicionados no repositório."},
    GITADD:            Instruction{"Para adicionar algo que foi modificado ou criado, pode ser utilizado para adicionar arquivos e diretórios."},
    GITCOMMIT:         Instruction{"Para gravar as alterações do que foi modificado, adicionado ou removido."},
    GITPULL:           Instruction{"Este comando mescla as alterações do repositório remoto para o seu repositório local, ou seja, ele traz todas as modificações que foram efetuadas."},
    GITCLONE:          Instruction{"Permite clonar um repositório existente. Clonar é basicamente extrair uma cópia completa de todos os dados de um repositório para seu diretório."},
}
